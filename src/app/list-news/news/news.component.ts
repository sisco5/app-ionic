import { Component, OnInit } from '@angular/core';
import { NewsService } from "../../services/news.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-new',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {

  News: any[] = [];
  errorMessage: string;

  constructor(private newsService: NewsService, private route: ActivatedRoute ) { }

  ngOnInit() {
    // Get the news id
    const param = this.route.snapshot.paramMap.get('id');
    if (param) {
      const id = +param;
      this.getNews(id);
    }
  }

  getNews(id: number) {
    // Get the news
    this.newsService.getNews().subscribe((data:any) => {
      const NEWS = data.page.items;
      this.News = NEWS.filter(
          news => news.id == id );
    }, error => {
      this.errorMessage = error;
    });
  }

}
