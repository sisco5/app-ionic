import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  url = 'http://www.rtve.es/api/noticias.json';

  constructor(private http: HttpClient) { }

  // Function to get the news
  getNews() {
    return this.http.get(this.url);
  }

}
