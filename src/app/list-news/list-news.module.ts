import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListNewsPage } from './list-news.page';
import { NewsComponent } from "./news/news.component";

const routes: Routes = [
  {
    path: '',
    component: ListNewsPage
  },
  { path: ':id',
    component: NewsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListNewsPage, NewsComponent]
})
export class ListNewsPageModule {}
