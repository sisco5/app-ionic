import { Component, OnInit } from '@angular/core';
import { NewsService } from "../services/news.service";

@Component({
  selector: 'app-list-news',
  templateUrl: './list-news.page.html',
  styleUrls: ['./list-news.page.scss'],
})
export class ListNewsPage implements OnInit {

  News: any;
  errorMessage: string;

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    // Obtaining the news
    this.newsService.getNews()
        .subscribe(albums => {
              this.News = albums;
            },
            error => this.errorMessage = <any>error);
  }

}
